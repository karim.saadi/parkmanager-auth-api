export enum AuthRole {
    ADMIN = 'admin',
    USER = 'user',
}