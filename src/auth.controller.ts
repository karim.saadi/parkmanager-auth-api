import { Body, Controller, Get, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SigninSuccessDTO } from './dtos/signin-success.dto';
import { SigninDTO } from './dtos/signin.dto';
import { SignupSuccessDTO } from './dtos/signup-succes.dto';
import { SignupDTO } from './dtos/signup.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post('signin')
  async signin(@Body() signinDTO: SigninDTO): Promise<SigninSuccessDTO> {
    return this.authService.signin(signinDTO);
  }

  @Post('signup')
  async signup(@Body() signinDTO: SignupDTO): Promise<SignupSuccessDTO> {
    return this.authService.signup(signinDTO);
  }
}
