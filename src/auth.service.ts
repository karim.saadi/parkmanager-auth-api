import { BadRequestException, Inject, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { ClientProxy } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { compare, hash } from 'bcrypt';
import { Auth } from 'parkmanager-models/dist/auth.model';
import { User } from 'parkmanager-models/dist/user.model';
import { firstValueFrom } from 'rxjs';
import { Repository } from 'typeorm';
import { SigninSuccessDTO } from './dtos/signin-success.dto';
import { SigninDTO } from './dtos/signin.dto';
import { SignupSuccessDTO } from './dtos/signup-succes.dto';
import { SignupDTO } from './dtos/signup.dto';
import { AuthRole } from './enums/auth-role.enum';
import { ApiReponsesMessages } from './utils/api-responses.util';

@Injectable()
export class AuthService {

  constructor(
    @InjectRepository(Auth)
    private authRepository: Repository<Auth>,
    @Inject('RMQ_USERS_SERVICE')
    private client: ClientProxy,
    private jwtService: JwtService,
    private configService: ConfigService
  ) {
    this.syncClient()
  }

  async syncClient(): Promise<void> {
    await this.client.connect()
      .then(console.log)
      .catch(console.error);
  }

  async signin(signinDTO: SigninDTO): Promise<SigninSuccessDTO> {
    const foundAuth = await this.authRepository.findOneBy({
      email: signinDTO.email
    });

    if (!foundAuth) {
      throw new NotFoundException(ApiReponsesMessages.NOT_FOUND)
    }

    const isSamePassword = await compare(signinDTO.password, foundAuth.password);

    if (!isSamePassword) {
      throw new BadRequestException(ApiReponsesMessages.WRONG_PASSWORD)
    }

    try {
      const foundUser = await firstValueFrom(
        this.client.send<User>('get-one-user', {
          userId: foundAuth.userId,
          headers: {
            authorization: 'Bearer ' + this.jwtService.sign({},
              {
                expiresIn: '1s',
                secret: this.configService.get('JWT_SECRET_KEY')
              })
          }
        })
      );

      const signinSuccessDTO = new SigninSuccessDTO();
      signinSuccessDTO.token = this.jwtService.sign(
        {
          userId: foundUser.id
        },
        {
          expiresIn: '1d',
          secret: this.configService.get('JWT_SECRET_KEY')
        });
      signinSuccessDTO.userId = foundUser.id;
      signinSuccessDTO.role = AuthRole.USER;
      return signinSuccessDTO;
    } catch (error) {
      console.error(error);
      throw new InternalServerErrorException(ApiReponsesMessages.QUEUE_ERROR)
    }
  }

  /**
   * @description signup new user
   * @param signupDTO 
   * @returns {SignupSuccessDTO}
   */
  async signup(signupDTO: SignupDTO): Promise<SignupSuccessDTO> {
    const user = await this.authRepository.findOneBy({
      email: signupDTO.email
    });

    if (user) {
      throw new BadRequestException(ApiReponsesMessages.ALREADY_EXISTS)
    }

    const hashedPassword = await hash(signupDTO.password, 10);

    try {
      const createdUser = await firstValueFrom(
        this.client.send<User>('create-one-user', {
          username: signupDTO.username,
          headers: {
            authorization: 'Bearer ' + this.jwtService.sign({},
              {
                expiresIn: '1s',
                secret: this.configService.get('JWT_SECRET_KEY')
              })
          }
        })
      )

      const newAuth = new Auth();
      newAuth.email = signupDTO.email;
      newAuth.password = hashedPassword;
      newAuth.userId = createdUser.id;
      const savedAuth = await this.authRepository.save(newAuth)
      return savedAuth;
    } catch (error) {
      console.error(error);
      throw new InternalServerErrorException(ApiReponsesMessages.QUEUE_ERROR)
    }
  }
}
