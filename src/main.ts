import { NestFactory } from '@nestjs/core';
import helmet from 'helmet';
import { ConfigService } from '@nestjs/config';
import { AuthModule } from './auth.module';
import * as compression from 'compression';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AuthModule);
  const configService = app.get(ConfigService);
  const config = new DocumentBuilder()
    .setTitle('Parkmanager Auth API')
    .setDescription('The parkmanager auth API description')
    .setVersion('1.0')
    .addTag('auth')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  const service = await NestFactory.createMicroservice<MicroserviceOptions>(AuthModule, {
    transport: Transport.RMQ,
    options: {
      urls: [`amqp://${configService.get('RMQ_HOST')}:${configService.get('RMQ_PORT')}`],
      queue: `${configService.get('RMQ_QUEUE')}`,
      queueOptions: {
        durable: false
      },
    },
  });

  app.use(helmet());
  app.enableCors();
  app.use(compression());

  await app.listen(configService.get('APP_PORT') || 3002);

  await service.listen()
    .then(() => console.log('service MQ running'))
    .catch(console.error);
}
bootstrap();
