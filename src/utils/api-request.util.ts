export const API_HEADER_SWAGGER =  {
    name: 'Authorization',
    allowEmptyValue: false,
    description: 'Bearer token'
}