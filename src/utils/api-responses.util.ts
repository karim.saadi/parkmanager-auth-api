export enum ApiReponsesMessages {
    NOT_FOUND = 'Utilisateur non trouvé',
    WRONG_PASSWORD = 'Le mot de passe est incorrect',
    ALREADY_EXISTS = 'Un compte avec cet email existe déjà',
    QUEUE_ERROR = 'Un problème est survenu',
    FORBIDDEN = 'Vous n\'avez pas le droit d\'accéder à cette ressource',
    UNAUTHORIZED = 'Vous n\'êtes pas autorisé à accéder à cette ressource',
}