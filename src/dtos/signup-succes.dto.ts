import { IsEmail } from "class-validator";

export class SignupSuccessDTO {
    @IsEmail()
    email: string;
}