import { AuthRole } from "src/enums/auth-role.enum";

export class SigninSuccessDTO {
    token: string;
    userId: number;
    role: AuthRole;
}